/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.hash;

import java.util.ArrayList;

/**
 *
 * @author Thanya
 */
public class Hash {
    ArrayList<String> table = new ArrayList<String>();
    
    public Hash(int n) {
        for(int i=0; i<n; i++) {
            table.add(i, null);
        }
    }
    
    private int hash(int key) {
        return key % table.size();
    }
    
    public void put(int key, String value) {
        table.set(hash(key), value);
    }
    
    public String get(int key) {
        return table.get(hash(key));
    }
    
    public void remove(int key) {
        table.set(hash(key), null);
    }
    
    
}
