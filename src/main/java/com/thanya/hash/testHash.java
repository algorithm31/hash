/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.hash;

/**
 *
 * @author Thanya
 */
public class testHash {
    public static void main(String[] args) {
        
        Hash h = new Hash(15);
        h.put(25, "Mamuang");
        h.put(18, "Kao");
        h.put(39, "Baba");
        h.put(15, "Mook");
        h.put(29, "Lala");
        
        System.out.println(h.get(25));
        System.out.println(h.get(18));
        System.out.println(h.get(39));
        System.out.println(h.get(15));
        System.out.println(h.get(29));
        System.out.println("remove key 15");
        System.out.println(h.get(15));
    }
}
